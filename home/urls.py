from os import name
from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('data/', views.sebuah_fungsi, name='sebuah_fungsi')
]