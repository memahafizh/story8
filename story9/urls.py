from os import name
from django.urls import path
from django.urls.conf import include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.awal, name='awal'),
    path('register/', views.register, name='register'),
]