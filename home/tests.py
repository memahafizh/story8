from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import index, sebuah_fungsi
from .apps import HomeConfig
# Create your tests here.

class Home(TestCase):
    def test_index_is_exist(self):
        response = Client().get('/home/')
        self.assertEquals(response.status_code, 200)

    def test_data_is_exist(self):
        response = Client().get('/home/data/?q=')
        self.assertEquals(response.status_code, 200)

    def test_app_test(self):
        self.assertEquals(HomeConfig.name, 'home')