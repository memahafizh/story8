from django.shortcuts import redirect, render
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def awal(request):
    return render(request, 'awal.html')

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')  
    else:
        form = UserCreationForm()
    return render(request, 'reg.html', {'form' : form})
    