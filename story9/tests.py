from django.contrib.auth.forms import UserCreationForm
from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import awal, register
from .apps import Story9Config
# Create your tests here.

class Story9(TestCase):
    def test_index_is_exist(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_reg_is_exist(self):
            response = Client().get('/register/')
            self.assertEquals(response.status_code, 200)
            
    def test_app_test(self):
        self.assertEquals(Story9Config.name, 'story9')

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_POST_reg(self):
        response = self.client.post('/register/', {
            'username': 'Hafizh',
            'password1': 'PPWasikuhuy',
            'password2': 'PPWasikuhuy'
        }, follow=True)
        self.assertEqual(response.status_code, 200)